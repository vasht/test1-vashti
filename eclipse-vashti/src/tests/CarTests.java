package tests;
import vehicles.Car;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {

	//Checks if constructor throws
	@Test
	void testCarConstructorValidation() {
		try {
			Car c = new Car(-5);
			fail("Car constructor did not throw an exception");
		}
		catch (IllegalArgumentException e){
			//Expected
			//Do nothing
		}
		catch (Exception e) {
			fail("Exception is thrown but not the expected exception");
		}
	}
	//Checks if getLocation() returns correct value
	@Test
	void testLocation() {
		Car c = new Car(137); //location should start at 0
		int expected = 0;
		int result = c.getLocation();
		assertEquals(expected,result);
	}
	//Checks if getSpeed() returns correct value
	@Test
	void testSpeed() {
		Car c = new Car(137);
		int expected = 137;
		int result = c.getSpeed();
		assertEquals(expected,result);
	}
	//Checks if moveLeft() subtracts speed from location(location-speed)
	@Test
	void testMoveLeft() {
		Car c = new Car(137); //initial location is 0
		c.moveLeft();
		int expected = -137;
		int result = c.getLocation();
		assertEquals(expected,result);
		
		c.moveLeft();
		expected = -274;
		result = c.getLocation();
		assertEquals(expected,result);
	}
	//Checks if moveRights() adds speed from location(location+speed)
	@Test
	void testMoveRight() {
		Car c = new Car(137); //initial location is 0
		c.moveRight();
		int expected = 137;
		int result = c.getLocation();
		assertEquals(expected,result);
		
		c.moveRight();
		expected = 274;
		result = c.getLocation();
		assertEquals(expected,result);
	}
	//Checks if accelerate() increments speed by 1
	@Test
	void testAccelerate() {
		Car c = new Car(137);
		c.accelerate();
		int expected = 138;
		int result = c.getSpeed();
		assertEquals(expected,result);
	}
	//Checks if decelerate() subtracts speed by 1
	@Test
	void testDecelerate() {
		Car c = new Car(137);
		c.decelerate();
		int expected = 136;
		int result = c.getSpeed();
		assertEquals(expected,result);
	}
	//Checks if decelerate() prevents speed from going negative
	@Test
	void testDecelerateValidation() {
		Car c = new Car(0);
		c.decelerate();
		int expected = 0;
		int result = c.getSpeed();
		assertEquals(expected,result);
	}
}
