package tests;
import utilities.MatrixMethod;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MatrixMethodTests {
	//Checks if rectangular array is duplicated properly
	@Test
	void testDuplicateRectangular() {
		int[][] arr = {{1,2,3},{-4,5,6}};
		int [][] expected = {{1,2,3,1,2,3},{-4,5,6,-4,5,6}};
		int[][] result = MatrixMethod.duplicate(arr);
		assertArrayEquals(expected,result);
	}
	//Checks if square array is duplicated properly
	@Test
	void testDuplicateSquare() {
		int[][] arr = {{-1,2,3},{4,-5,6},{7,8,-11}};
		int[][] expected = {{-1,2,3,-1,2,3},{4,-5,6,4,-5,6},{7,8,-11,7,8,-11}};
		int[][] result = MatrixMethod.duplicate(arr);
		assertArrayEquals(expected,result);
	}
	//Checks if 2d array with inner array length of 1 is duplicated properly
	@Test
	void testDuplicateOneInnerValueArray() {
		int[][] arr = {{1},{2}};
		int[][] expected = {{1,1},{2,2}};
		int[][] result = MatrixMethod.duplicate(arr);
		assertArrayEquals(expected,result);
	}
	//Checks if 2d array with only 1 array is duplicated properly
	@Test
	void testDuplicateOneInnerArray() {
		int[][] arr = {{1,2,3}};
		int[][] expected = {{1,2,3,1,2,3}};
		int[][] result = MatrixMethod.duplicate(arr);
		assertArrayEquals(expected,result);
	}
}
