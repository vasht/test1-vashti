package utilities;

public class MatrixMethod {
	//Creates and duplicates a given 2d array's values
	public static int[][] duplicate(int[][] arr) {
		//The inner array length of dupArr is double the inner array length of arr since the values are duplicated
		int[][] dupArr = new int[arr.length][arr[0].length*2];
		
		for(int i = 0; i < dupArr.length; i++) {
			int a = 0; //arr inner index
			for(int j = 0; j < dupArr[i].length; j++) {
				//Resets arr index to 0 in order to reiterate arr again
				if(a >= arr[i].length) {
					a = 0;
				}
				dupArr[i][j] = arr[i][a];
				a++;
			}
		}
		return dupArr;
	}
}
